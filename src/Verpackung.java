public class Verpackung {
	
	private String Bezeichnung;
	private double Volumen;
	private double Pfand;

	public Verpackung(String bez, double vl, double pf){
		this.Bezeichnung = bez;
		this.Volumen = vl;
		this.Pfand = pf;
	}
	
	//Getter/Setter
	public String getBezeichnung() {
		return Bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.Bezeichnung = bezeichnung;
	}
	public double getVolumen() {
		return Volumen;
	}
	public void setVolumen(double volumen) {
		this.Volumen = volumen;
	}
	public double getPfand() {
		return Pfand;
	}
	public void setPfand(double pfand) {
		Pfand = pfand;
	}

}
