import java.util.Calendar;

public class Lagereinheit {
	
	private int AnzahlAufEinheit;
	private Calendar AblaufDatum;
	private Getraenk Getränketyp;
	
	public Lagereinheit(int anzahlAufEinheit,
			Calendar ablaufDatum, Getraenk getränketyp) {
		AnzahlAufEinheit = anzahlAufEinheit;
		AblaufDatum = ablaufDatum;
		Getränketyp = getränketyp;
	}
	
	//Getter/Setter
	public int getAnzahlAufEinheit() {
		return AnzahlAufEinheit;
	}
	public void setAnzahlAufEinheit(int anzahlAufEinheit) {
		AnzahlAufEinheit = anzahlAufEinheit;
	}
	public Calendar getAblaufDatum() {
		return AblaufDatum;
	}
	public void setAblaufDatum(Calendar ablaufDatum) {
		AblaufDatum = ablaufDatum;
	}
	public Getraenk getGetränketyp() {
		return Getränketyp;
	}
	public void setGetränketyp(Getraenk getränketyp) {
		Getränketyp = getränketyp;
	}
	
	

}
