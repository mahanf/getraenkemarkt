import java.util.ArrayList;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;


public class Lager {
	
	private int MaxKiste;
	private int MaxPalette;
	private ArrayList<Kiste> IstKiste;
	private ArrayList<Palette> IstPalette;
	
	
	public Lager(int maxKiste, int maxPalette) {
		MaxKiste = maxKiste;
		MaxPalette = maxPalette;
		IstKiste = new ArrayList<Kiste>();
		IstPalette = new ArrayList<Palette>();

	}
	
	public void kisteHinzufügen(Kiste k){
		IstKiste.add(k);
		k.getGetränketyp().erhoeheBestand(k.getAnzahlAufEinheit());
	}
	public void paletteHinzufügen(Palette p){
		IstPalette.add(p);
		p.getGetränketyp().erhoeheBestand(p.getAnzahlAufEinheit());
	}
	public Set<Getraenk> sortimentErzegen(){
		
		Comparator<Getraenk> comp = new Comparator<Getraenk>() {
			@Override
			public int compare(Getraenk o1, Getraenk o2) {
				if(o1.getId()< o2.getId())					
					return -1;
				if(o1.getId()> o2.getId())					
					return 1;
				else
					return 0;
			}		
		};
		
		Set<Getraenk> sortiment = new TreeSet<Getraenk>(comp);
		
		for (Lagereinheit lag: IstKiste){
			sortiment.add(lag.getGetränketyp());
		}
		for (Lagereinheit lag: IstPalette){
			sortiment.add(lag.getGetränketyp());
		}
		
		return sortiment;
		
	}

	
	
	//Getter/Setter
	public int getMaxKiste() {
		return MaxKiste;
	}
	public void setMaxKiste(int maxKiste) {
		MaxKiste = maxKiste;
	}
	public int getMaxPalette() {
		return MaxPalette;
	}
	public void setMaxPalette(int maxPalette) {
		MaxPalette = maxPalette;
	}
	public ArrayList<Kiste> getIstKiste() {
		return IstKiste;
	}
	public void setIstKiste(ArrayList<Kiste> istKiste) {
		IstKiste = istKiste;
	}
	public ArrayList<Palette> getIstPalette() {
		return IstPalette;
	}
	public void setIstPalette(ArrayList<Palette> istPalette) {
		IstPalette = istPalette;
	}
	
	

}
