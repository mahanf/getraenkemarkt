import java.util.Calendar;
import java.util.GregorianCalendar;


public class main {

	public static void main(String[] args) {
		Verpackung doseKl = new Verpackung("Dose klein", 0.33, 0.25);
		Verpackung flascheKl = new Verpackung("Flasche klein", 0.33, 0.08);
		
		//Angebot des Marktes 
		Getraenk co1 = new Getraenk(1,"Cola",doseKl,0.99);
		Getraenk co2 = new Getraenk(2,"Cola",flascheKl,0.99);
		Getraenk co3 = new Getraenk(3,"Fanta",flascheKl,0.99);
		Getraenk bi1 = new Getraenk(4,"Astra",flascheKl,0.59);
		Getraenk bi2 = new Getraenk(5,"Becks",flascheKl,0.89);
		
		//Vorrat des Marktes
		//Sollte automatisiert (Scanner) erfolgen
		Kiste k1 = new Kiste(24, new GregorianCalendar(2020,1,1),bi1,1.5);
		Kiste k2 = new Kiste(24, new GregorianCalendar(2020,2,1),co2,1.5);
		Kiste k3 = new Kiste(24, new GregorianCalendar(2020,4,1),bi1,1.5);
		Kiste k4 = new Kiste(24, new GregorianCalendar(2020,4,1),bi2,1.5);
		Kiste k5 = new Kiste(24, new GregorianCalendar(2020,8,1),bi2,1.5);
		Kiste k6 = new Kiste(24, new GregorianCalendar(2020,8,1),co3,1.5);
		Palette p1 = new Palette(20, new GregorianCalendar(2020,4,1), co1);
		
		Lager lager = new Lager(100,100);
		
		lager.kisteHinzufügen(k1);
		lager.kisteHinzufügen(k2);
		lager.kisteHinzufügen(k3);
		lager.kisteHinzufügen(k4);
		lager.kisteHinzufügen(k5);
		lager.kisteHinzufügen(k6);
		lager.paletteHinzufügen(p1);
		
		//System.out.println(co1.getBezeichnung()+" "+ co1.getVerpackung().getBezeichnung() +" "+ co1.getBestand());
		//System.out.println(co2.getBezeichnung()+" "+ co2.getVerpackung().getBezeichnung() +" "+ co2.getBestand());
		//System.out.println(bi1.getBezeichnung()+" "+ bi1.getVerpackung().getBezeichnung() +" "+ bi1.getBestand());
		//System.out.println();
		
		Kasse kasse = new Kasse();
		
		ConsolenDialog cd = new ConsolenDialog();
		cd.startDialog(lager, kasse);
		
		//kasse.getränkHinzufuegen(bi1, 2);
		//kasse.getränkHinzufuegen(co2, 4);
		//kasse.abrechnen();


		
		
	}

}
