import java.util.Scanner;
import java.util.Set;


public class ConsolenDialog {

	Scanner scan = new Scanner(System.in);
	
	public void startDialog(Lager l, Kasse k){
		boolean x = true;

		while (x){
			System.out.println("Wilkommen!");
			System.out.println("1 - Getrnke-Bestand anzeigen");
			System.out.println("2 - Einkauf eingeben");
			System.out.println("3 - Einkauf anzeigen");
			System.out.println("4 - Einkauf abrechnen");
			System.out.println("0 - Schliessen");			
			
			int input = scan.nextInt();
			
			switch(input){
			case 1:
				bestandAnzeigen(l);
				break;
			case 2:
				einkaufEingeben(k,l);
				break;
			case 3:
				einkaufAnzeigen(k);
				break;
			case 4:
				k.abrechnen();
				break;
			case 0:
				x = false;
				break;
			}
		}
	}
	
	public void bestandAnzeigen(Lager l){
		Set<Getraenk> sortiment = l.sortimentErzegen();
		for(Getraenk g: sortiment){
			System.out.println(g.getId()+" "+g.getBezeichnung() +" "+ g.getBestand());
		}
		System.out.println();
		
	}
	public void einkaufEingeben(Kasse k, Lager l){
		boolean x = true;
		while(x){
			bestandAnzeigen(l);
			System.out.println("Whlen Sie die Id aus: ");
			int id = scan.nextInt();
			Getraenk ge = new Getraenk();
			for (Getraenk so: l.sortimentErzegen()){
				if(so.getId() == id){
					ge = so;
					break;
				}
			}
			System.out.println("Geben Sie die Anzahl ein: ");
			int anz = scan.nextInt();
			k.getrnkHinzufuegen(ge, anz);
			einkaufAnzeigen(k);	
			System.out.println("Weiteres Getrnk?(Y/N)");
			String repeat = scan.next();
			x = repeat.equals("Y");
		}
		
	}
	public void einkaufAnzeigen(Kasse k){
		for (Posten p : k.getGetraenkeListe()){
			System.out.println(p.getGetraenk().getBezeichnung()+ " " + p.getAnzahl());
		}
		System.out.println();
		
	}
}
