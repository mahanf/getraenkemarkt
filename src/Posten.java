
public class Posten {
	private Getraenk Getraenk;
	private int Anzahl;
	
	public Posten(Getraenk getraenk, int anzahl) {
		Getraenk = getraenk;
		Anzahl = anzahl;
	}
	
	
	public Getraenk getGetraenk() {
		return Getraenk;
	}
	public void setGetraenk(Getraenk getraenk) {
		Getraenk = getraenk;
	}
	public int getAnzahl() {
		return Anzahl;
	}
	public void setAnzahl(int anzahl) {
		Anzahl = anzahl;
	}
	

}
