import java.util.Calendar;
import java.util.Date;


public class Kiste extends Lagereinheit{
	
	private double Pfandwert;

	public Kiste(int anzahlAufEinheit,
			Calendar ablaufDatum, Getraenk getränketyp, double pfandwert) {
		super(anzahlAufEinheit, ablaufDatum, getränketyp);
		Pfandwert = pfandwert;
	}
	
	//Getter/Setter
	public double getPfandwert() {
		return Pfandwert;
	}
	public void setPfandwert(double pfandwert) {
		Pfandwert = pfandwert;
	}

}
