import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Rechnung {
	
	private ArrayList<Posten> GetraenkeListe;
	private Calendar Rechnungsdatum;
	private double Summe;
	
	
	//Getter/Setter
	public ArrayList<Posten> getGetraenkeListe() {
		return GetraenkeListe;
	}
	public void setGetraenkeListe(ArrayList<Posten> getraenkeListe) {
		GetraenkeListe = getraenkeListe;
	}
	public Calendar getRechnungsdatum() {
		return Rechnungsdatum;
	}
	public void setRechnungsdatum(Calendar rechnungsdatum) {
		Rechnungsdatum = rechnungsdatum;
	}
	public double getSumme() {
		return Summe;
	}
	public void setSumme(double summe) {
		Summe = summe;
	}
	

}
