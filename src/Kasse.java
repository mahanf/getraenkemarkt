import java.util.ArrayList;
import java.util.GregorianCalendar;

public class Kasse {
	
	private ArrayList<Posten> GetraenkeListe = new ArrayList<Posten>();
	private ArrayList<Rechnung> Rechnungen = new ArrayList<Rechnung>();
	private double Summe;
	
	public void rechnungDrucken(){
		Rechnung r = new Rechnung();
		r.setGetraenkeListe(GetraenkeListe);
		r.setSumme(Summe);
		r.setRechnungsdatum(new GregorianCalendar());
		Rechnungen.add(r);
		
	}
	public void abrechnen(){
		Summe = 0;
		for(Posten p: GetraenkeListe){
			Summe += p.getGetraenk().getPreis()*p.getAnzahl();
			p.getGetraenk().verringereBestand(p.getAnzahl());
		}
		System.out.println("Bitte Zahlen sie: "+Summe);
		rechnungDrucken();
	}
	public double tagesumsatz(){
		return 0;
	}
	public void getränkHinzufuegen(Getraenk g, int anz){
		GetraenkeListe.add(new Posten(g,anz));

	}
	
	//Getter/Setter
	public ArrayList<Rechnung> getRechnungen() {
		return Rechnungen;
	}
	public void setRechnungen(ArrayList<Rechnung> rechnungen) {
		Rechnungen = rechnungen;
	}
	public ArrayList<Posten> getGetraenkeListe() {
		return GetraenkeListe;
	}
	public void setGetraenkeListe(ArrayList<Posten> getraenkeListe) {
		GetraenkeListe = getraenkeListe;
	}

}
