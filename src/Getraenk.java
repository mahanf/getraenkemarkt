
public class Getraenk {
	
	private int Id;
	private String Bezeichnung;
	private Verpackung Verpackung;
	private double Preis;
	private int Bestand;
	
	public Getraenk(){

	}
	
	public Getraenk(int id, String bez, Verpackung vp, double preis){
		this.Id=id;
		this.Bezeichnung = bez;
		this.Verpackung = vp;
		this.Preis = preis;
		this.Bestand = 0;
	}
	
	public void erhoeheBestand(int val){
		this.Bestand += val;
	}
	public void verringereBestand(int val){
		this.Bestand -= val;
	}
	
	//Getter/Setter
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getBezeichnung() {
		return Bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		Bezeichnung = bezeichnung;
	}
	public Verpackung getVerpackung() {
		return Verpackung;
	}
	public void setVerpackung(Verpackung verpackung) {
		Verpackung = verpackung;
	}
	public double getPreis() {
		return Preis;
	}
	public void setPreis(double preis) {
		Preis = preis;
	}
	public int getBestand() {
		return Bestand;
	}
	public void setBestand(int bestand) {
		Bestand = bestand;
	}
}
